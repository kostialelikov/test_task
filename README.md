# Test task
### Lelikov Kostiantyn

---
# How to run (Docker)
```
docker-compose up
```

---
# How to run (Without docker)
* Install dependencies
```
npm intall
```
* Start master
```
npm run master
```
* Then start agent
```
npm run agent
```
