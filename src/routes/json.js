const needle = require('needle');
const logger = require('../helpers/logger');
const cheerio = require('cheerio');

const json =  (socket) => async (data) => {
    const ip = socket.handshake.address;
    const userAgent = socket.client.conn.request.headers['user-agent'];
    try {
        const obj = typeof data === "string" ? JSON.parse(data) : data;
        const {query} = obj;
        if (query) {
            const searchStr = query.replace(' ', '+');
            const response = await needle('get', `https://www.google.com/search?q=${searchStr}&amp;oq=${searchStr}&amp;sourceid=chrome&amp;ie=UTF-8`, {
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36'
                }
            });
            const $ = cheerio.load(response.body);
            const countString = $('#resultStats').contents()[0].data;
            const count = countString.replace(/\D/g, '');
            logger.requests.debug(`${ip} "${userAgent}" - New JSON: ${JSON.stringify(data)}, About ${count} results`);
        } else {
            logger.requests.debug(`${ip} "${userAgent}" - New JSON: ${JSON.stringify(data)}`);
        }
        socket.emit('success', 200);
    } catch (e) {
        socket.error(e);
    }
};

module.exports = json;
