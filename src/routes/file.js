const fs = require('promise-fs');
const path = require('path');
const uuid = require('uuid/v4');
const fileType = require('file-type');
const logger = require('../helpers/logger');

const FILES_DIR = path.join(path.dirname(fs.realpathSync(__filename)), '../../public/files');

const file = (socket) => async (data) => {
    try {
        const fileDesc = fileType(data);
        if (!fileDesc && !fileDesc.mime.includes('image')) {
            socket.error(new Error('File must be image'));
            return;
        }
        const newFileName = `${uuid()}.${fileDesc.ext}`;
        await fs.writeFile(FILES_DIR + '/' + newFileName, data);
        const ip = socket.handshake.address;
        const userAgent = socket.client.conn.request.headers['user-agent'];
        logger.requests.debug(`${ip} "${userAgent}" - New Image: http://${process.env.HOST || 'localhost'}:${process.env.POST || 3000}/files/${newFileName}`);
        socket.emit('success', 200);
    } catch (e) {
        socket.error(e);
    }
};

module.exports = file;
