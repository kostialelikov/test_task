const json = require('./json');
const raw = require('./raw');
const file = require('./file');

module.exports = {
    json,
    raw,
    file
};
