const logger = require('../helpers/logger');

const raw = (socket) => (data) => {
    const ip = socket.handshake.address;
    const userAgent = socket.client.conn.request.headers['user-agent'];
    logger.requests.debug(`${ip} "${userAgent}" - New RAW: ${data}`);
    socket.emit('success', 200);
};

module.exports = raw;
