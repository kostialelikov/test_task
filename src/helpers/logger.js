const log4js = require('log4js');
const config = require('../../config');

log4js.configure(config.log4js);

module.exports = {
    connections: log4js.getLogger('connections'),
    requests: log4js.getLogger('requests')
};

