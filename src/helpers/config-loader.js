const path = require('path');
const Joi = require('joi');

const DEFAULT_JOI_OPTIONS = {
    allowUnknown: true,
    convert: true,
    presence: 'required'
};

const DEFAULT_CONFIG_PATTERN = 'config.#env';


module.exports = (configDirectory, joiSchema, options = {}, pattern = DEFAULT_CONFIG_PATTERN) => {
    const env = process.env.NODE_ENV || 'development';
    const configFileName = path.join(configDirectory, pattern.replace('#env', env));
    const config = require(configFileName);
    const {error, value} = Joi.validate(config, joiSchema, {...DEFAULT_JOI_OPTIONS, ...options});
    if (error) throw new Error(`Config load error: ${error.message}`);
    return value;
};
