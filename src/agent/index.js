const App = require('./app');

const app = new App();
app.setup();

setInterval(async () => {
    if (!app.error) {
        const random = Math.floor(Math.random() * 3);
        switch (random) {
            case 0: {
                app.sendJson({
                    query: 'Node js',
                    abc: 'abc'
                });
                break;
            }
            case 1: {
                app.sendRaw('Some raw data');
                break;
            }
            case 2: {
                const imageName = `${Math.floor(Math.random() * (3 - 1) + 1)}.jpg`;
                await app.sendFile(__dirname + '/example-images/' + imageName);
            }
        }
    }
}, 4000);
