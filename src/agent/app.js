const io = require('socket.io-client');
const fs = require('promise-fs');
const config = require('../../config');

class App {
    constructor() {
        this.socket = null;
        this.lastOperation = null;
        this.error = false;
    }

    setup() {
        this.socket = io.connect(`http://${config.conf.app.host}:${config.conf.app.port}`, {
            reconnection: true,
            reconnectionDelay: 1000,
            reconnectionDelayMax: 5000,
            reconnectionAttempts: Infinity
        });
        this.socket.on('connect', async () => {
            console.info('Connected to server ' + this.socket.id);
            if (this.lastOperation && this.error) {
                await this.retry();
                this.error = false;
            }
        });
        this.socket.on('disconnect', () => {
            console.error('Connection terminated. Reconnection attempt.')
        });
        this.socket.on('error', async () => {
            console.error('Retrying last operation');
            this.error = true;
            await this.retry();
        });
        this.socket.on('success', () => {
            this.lastOperation = null;
            this.error = false;
        });
    }

    async retry() {
        if (this.lastOperation) {
            switch (this.lastOperation.operation) {
                case 'JSON': {
                    this.sendJson(...this.lastOperation.args);
                    break;
                }
                case 'RAW': {
                    this.sendRaw(...this.lastOperation.args);
                    break;
                }
                case 'FILE': {
                    await this.sendFile(...this.lastOperation.args);
                    break;
                }
            }
        }
        this.lastOperation = null;
    }

    sendJson(object = {}) {
        this.socket.emit('json', JSON.stringify(object));
        this.lastOperation = {
            operation: 'JSON',
            args: [object]
        }
    }

    sendRaw(raw) {
        this.socket.emit('raw', raw);
        this.lastOperation = {
            operation: 'RAW',
            args: [raw]
        }
    }

    async sendFile(filename) {
        try {
            const data = await fs.readFile(filename);
            this.socket.emit('image', data);
            this.lastOperation = {
                operation: 'FILE',
                args: [filename]
            }
        } catch (e) {
            console.error(e.message);
        }
    }
}

module.exports = App;
