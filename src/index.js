const http = require('http');
const Server = require('socket.io');
const logger = require('./helpers/logger');
const routes = require('./routes/');
const nodeStatic = require('node-static');
const config = require('../config');

const file = new nodeStatic.Server('./public');

const server = http.createServer((req, res) => {
    req.addListener('end', function () {
        file.serve(req, res);
    }).resume();
});

const io = Server(server);

const listenCallback = () => {
    logger.connections.debug('Master is ready.');
};


server.listen(config.conf.app.port, listenCallback);

io.on('connection', socket => {
    const ip = socket.handshake.address;
    const userAgent = socket.client.conn.request.headers['user-agent'];
    logger.connections.debug(`${ip} "${userAgent}" - New connection`);

    socket.on('json', routes.json(socket));
    socket.on('raw', routes.raw(socket));
    socket.on('image', routes.file(socket));
});


