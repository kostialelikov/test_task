const Joi = require("joi");

module.exports = Joi.object({
    env: Joi.string()
        .valid(['production', 'development', 'test'])
        .default(process.env.NODE_ENV || 'development')
        .optional(),
    app: Joi.object({
        port: Joi.number(),
        host: Joi.string().default('localhost').optional()
    })
});
