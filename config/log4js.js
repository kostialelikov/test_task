const path = require('path');
const fs = require('fs');

const LOG_PATH = path.join(path.dirname(fs.realpathSync(__filename)), '../log');

const config = {
  appenders: {
    app: {
      type: 'stdout'
    },
    connections: {
      type: 'file',
      filename: LOG_PATH + '/connections.log'
    },
    requests: {
      type: 'file',
      filename: LOG_PATH + '/requests.log'
    }
  },
  categories: {
    default: {
      appenders: ['app'],
      level: 'debug'
    },
    connections: {
      appenders: ['app', 'connections'],
      level: 'debug'
    },
    requests: {
      appenders: ['app', 'requests'],
      level: 'debug'
    }
  }
};

module.exports = config;
