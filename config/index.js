const configLoader = require('../src/helpers/config-loader');
const schema = require('./config.schema');

module.exports = {
    log4js: require('./log4js'),
    conf: configLoader(__dirname, schema)
};
